const http = require('http');
const host = '127.0.0.1';
const porta = 8124;

const servidor = http.createServer((request, response) => {
  //let urlRequest = new URL(request.url, host);
  if(request.url === '/'){
  	homePage(request, response);
  }else if(request.url === '/outraPagina'){
    outraPagina(request, response);
  }else{
    response.statusCode = 404;
    response.setHeader('Content-Type','text/plain');
    response.end('Pagina nao encontrada!');
  }  
});

servidor.listen(porta, host, () => {
  console.log(`Servidor rodando em http://${host}:${porta}`);
});

function homePage(request, response){
  response.statusCode = 200;
  response.setHeader('Content-Type','text/html');
  response.end('<html><head><title>Titulo</title></head><body><h1>Pagina inicial</h1><p><a href=\'/outraPagina\'>Outra pagina</a></p></body></html>');
}

function outraPagina(request, response){
  response.statusCode = 200;
  response.setHeader('Content-Type','text/html');
  response.end('<html><head><title>Titulo</title></head><body><h1>Outra pagina</h1></body></html>');
}