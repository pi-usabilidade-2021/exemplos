const http = require('http');
const host = '127.0.0.1';
const porta = 8124;

const servidor = http.createServer((request, response) => {
  response.statusCode = 200;
  response.setHeader('Content-Type','text/plain');
  response.end('Ola mundo!');
});

servidor.listen(porta, host, () => {
  console.log(`Servidor rodando em http://${host}:${porta}`);
});
