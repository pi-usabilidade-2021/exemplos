const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const http = require('http');
const path = require('path');
const parser = require('body-parser');

const app = express();
const porta = 8126;
const banco = new sqlite3.Database('./usuario.db');

app.use(parser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname,'.')));

banco.run('CREATE TABLE IF NOT EXISTS usuario(id INTEGER NOT NULL,nome TEXT NOT NULL)');


//adiciona arquivo existente no diretório raiz
app.get('/',(request, response) => {
  response.sendFile(path.join(__dirname,'./form.html'));
});

//adiciona
app.post('/add',(request, response) => {
  banco.serialize(() => {
  	banco.run('INSERT INTO usuario(id,nome) VALUES(?,?)',[request.body.id,request.body.nome], (err) => {
  	  if(err){
  	  	response.send('Erro no acesso ao banco de dados');
  	  	return console.log(err.message);
  	  }
  	  console.log('Usuario cadastrado');
  	  response.send('Usuario '+request.body.nome+' cadastrado com id='+request.body.id);
  	});
  });
});

//consulta
app.post('/view',(request, response) => {
  banco.serialize(() => {
  	banco.each('SELECT * FROM usuario WHERE id=?',[request.body.id], (err,row) => {
  	  if(err){
  	  	response.send('Erro no acesso ao banco de dados');
  	  	return console.log(err.message);
  	  }
  	  console.log('Exibindo usuario');
  	  response.send(`Nome: ${row.nome}, id:${row.id}`);
  	});
  });
});
//fecha o banco
app.get('/close',(request, response) => {
	banco.close((err) => {
		if(err){
			response.send('Nao foi possivel encerrar a conexao');
			return console.log(err.message);
		}
		console.log('Conexao encerrada');
		response.send('Conexao com banco de dados encerrada');
	});
});

app.listen(porta, () => {
  console.log(`Servidor usando Express rodando na porta ${porta}!`);
});