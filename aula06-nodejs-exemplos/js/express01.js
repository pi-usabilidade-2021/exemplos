const express = require('express');
const app = express();
const porta = 8126;

app.get('/',(request, response) => {
  response.send('Ola mundo usando Express');
});

app.get('/about',(request, response) => {
  response.send('Sobre o Express');
});

app.listen(porta, () => {
  console.log(`Servidor usando Express rodando na porta ${porta}!`);
});