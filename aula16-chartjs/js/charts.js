var canvas = document.getElementById('grafico');

var grafico = new Chart(canvas, {
	type: 'bar',
	data: {
		labels: ['Java','JS','Python','C++','Ruby','Smalltalk'],
		datasets: [{
			label: 'Qtde de votos',
			data: [15,18,12,11,8,10],
			backgroundColor: ['red','green','yellow','blue','pink','gray'],
			borderColor: ['darkred','darkgreen','orange','navy','deeppink','black'],
			borderWidth: 3
		}]
	},
	options: {
		responsive: false,
		plugins: {
			title: {
				display: true,
				text: 'Linguagens preferidas',
				font: {
					size: 20
				}
			},
			legend: {
				labels:{
					font: {
						size: 16
					}
				}
			}
		},
		scales:{
			y: {
				title:{
					display: true,
					text: '# votos',
					font: {
						size: 18
					}
				}
			},
			x: {
				title:{
					display: true,
					text: 'linguagens',
					font: {
						size: 18
					}
				}
			}
		}
	}
});

var canvasLinha = document.getElementById('linha');

var graficoLinha = new Chart(canvasLinha, {
	type: 'line',
	data: {
		labels: ['Jan','Fev','Mar','Abr','Mai','Jun'],
		datasets: [{
			label: 'Viagens de carro (em milhares)',
			data: [5000,7500,8000,6000,9000,10000],
			borderColor: 'red',
			borderWidth: 3,
			tension: 0.1
		},{
			label: 'Viagens de bicicleta (em milhares)',
			data: [2000,2500,3200,2300,1900,1200],
			borderColor: 'blue',
			borderWidth: 3,
			tension: 0.1
		}]
	},
	options: {
		responsive: false,
		plugins: {
			title: {
				display: true,
				text: 'Viagens por modo de transporte',
				font: {
					size: 20
				}
			},
			legend: {
				labels:{
					font: {
						size: 16
					}
				}
			}
		},
		scales:{
			y: {
				title:{
					display: true,
					text: '# viagens',
					font: {
						size: 18
					}
				}
			},
			x: {
				title:{
					display: true,
					text: 'meses',
					font: {
						size: 18
					}
				}
			}
		}
	}
});

var canvasRosca = document.getElementById('rosca');

var graficoRosca = new Chart(canvasRosca, {
	type: 'doughnut',
	data: {
		labels: ['Buracos','Iluminação','Árvores','Ônibus'],
		datasets: [{
			label: 'Reclamações',
			data: [150,70,210,120],
			backgroundColor: ['blue','orange','green','black']
		}]
	},
	options: {
		responsive: false,
		plugins: {
			title: {
				display: true,
				text: 'Reclamações por categoria',
				font: {
					size: 20
				}
			},
			legend: {
				labels:{
					font: {
						size: 16
					}
				}
			}
		}
	}
});


var covidURL = 'covid';
var listaCovid = [];
var xhr = new XMLHttpRequest();

function carregarDados(){
	xhr.open('GET',covidURL,true);
	xhr.send();
}

xhr.onreadystatechange = function(){
	if(this.status == 200 && this.readyState == 4){
		listaCovid = JSON.parse(this.response);
		carregarGrafico();
	}
}

function pegarSemanas(){
	let sem = [];
	for(let i = 0;i < listaCovid.length;i++){
		sem.push(listaCovid[i].semana);
	}
	return sem;
}

function pegarObitos(){
	let obitos = [];
	for(let i = 0;i < listaCovid.length;i++){
		obitos.push(listaCovid[i].obitos);
	}
	return obitos;
}


function carregarGrafico(){

	let canvasCovid = document.getElementById('covid');
	let semanas = pegarSemanas();
	let obitos = pegarObitos();

	let graficoCovid = new Chart(canvasCovid, {
		type: 'line',
		data: {
			labels: semanas,
			datasets: [{
				label: 'Óbitos',
				data: obitos,
				borderColor: 'red',
				borderWidth: 3,
				tension: 0.1
			}]
		},
		options: {
			responsive: false,
			plugins: {
				title: {
					display: true,
					text: 'Óbitos no Brasil por semana em 2021 ',
					font: {
						size: 20
					}
				},
				legend: {
					labels:{
						font: {
							size: 16
						}
					}
				}
			},
			scales:{
				y: {
					title:{
						display: true,
						text: '# obitos',
						font: {
							size: 18
						}
					}
				},
				x: {
					title:{
						display: true,
						text: 'semanas',
						font: {
							size: 18
						}
					}
				}
			}
		}
	});


}



