//dependencias
const express = require('express');
const http = require('http');
const path = require('path');
const csv = require('csv-parser');
const fs = require('fs');

//configuracoes iniciais
const app = express();
const porta = 8126;

//app.use(parser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname,'.')));

app.get('/',(request, response) => {
  response.sendFile(path.join(__dirname,'./charts.html'));
});

var covid = [];
//le arquivo csv
function leArquivo(){
  fs.createReadStream('dados/brasil-obitos-2021-semana.csv')
    .on('error',()=> {})
    .pipe(csv())
    .on('data',(row) => covid.push(row))
    .on('end', () => {
      console.log('terminou');
    });
}

//página que carrega os dados cadaastrados
app.get('/covid',(request, response) => {
    if(covid.length == 0){
      leArquivo();
    }
    response.status(200).json(covid);
});

app.listen(porta, () => {
  console.log(`Servidor usando Express rodando na porta ${porta}!`);
});