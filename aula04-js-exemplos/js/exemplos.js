function mensagem(){
	alert("Olá mundo!");
}

function mudaTitulo(){
	let cabecalho = document.getElementById("titulo");
	cabecalho.innerHTML = 'Mudando o cabecalho da pagina!';
}

function conversao(){
	let valorStr = "5";
	if(valorStr === 5){
		alert("Converteu!");
	}
	else{
		alert("Não converteu!");
	}
}

function calculaArea(largura,altura){
	let area = largura * altura;
	return area;
}

function mostraArea(){
	prompt("Qual a área?");
}

function criaArray(){
	let cores = new Array('preto','branco','vermelho');
	cores.push('azul');
	alert('Tamanho do array: ' + cores.length);
}

function mouseSobreParagrafo(){
	let paragrafo = document.getElementById("paragrafo");
	paragrafo.onmouseover = function(){alert("Passou o mouse por cima do paragrafo")};
}

function geraNumero(){
	let numero = Math.floor((Math.random()*60)+1);
	alert("numero:" + numero);
}