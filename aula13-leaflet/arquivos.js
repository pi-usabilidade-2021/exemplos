//dependencias
const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const http = require('http');
const path = require('path');
const parser = require('body-parser');
const ejs = require('ejs');
const csv = require('csv-parser');
const fs = require('fs');

//configuracoes iniciais
const app = express();
const porta = 8126;
const banco = new sqlite3.Database('./dados/usuario.db');

app.use(parser.urlencoded({extended:false}));
app.use(express.static(path.join(__dirname,'.')));
app.set('view engine','ejs');

//scripts SQL
//cria tabela se ainda nao existir
const criarBanco = 'CREATE TABLE IF NOT EXISTS usuario(id INTEGER PRIMARY KEY AUTOINCREMENT, nome TEXT NOT NULL, email TEXT NOT NULL, login TEXT NOT NULL, senha TEXT NOT NULL)';
const criarBancoDados = 'CREATE TABLE IF NOT EXISTS dados(id INTEGER PRIMARY KEY AUTOINCREMENT, descricao TEXT NOT NULL, valor DECIMAL(10,2) NOT NULL)';
const incluirUsuario = 'INSERT INTO usuario(nome, email, login, senha) VALUES(?,?,?,?)';
const consultarUsuario = 'SELECT * FROM usuario WHERE login=? AND senha=?';
const incluirDados = 'INSERT INTO dados(descricao, valor) VALUES(?,?)';
const consultarDados = 'SELECT * FROM dados';


banco.run(criarBanco);
banco.run(criarBancoDados);

var usuario = '';
var dados = [];

//adiciona arquivo existente no diretório raiz
//esta é a página que vai aparecer ao acessar a aplicação
app.get('/',(request, response) => {
  response.sendFile(path.join(__dirname,'./views/login.html'));
});

//renderiza página principal
app.get('/main',(request, response) => {
  if(usuario != ''){
    response.render("main",{nome:usuario});
  }else{
    response.redirect('/');
  }
});

//consulta se usuário está cadastrado
app.post('/validate',(request, response) => {
  banco.serialize(() => {
    banco.get(consultarUsuario,[request.body.login,request.body.senha], (err,row) => {
      if(err){
        response.send('Erro no acesso ao banco de dados');
        return console.log(err.message);
      }
      if(row){
        usuario = row.login;
        response.redirect('main');
      }else{
        response.send('Não existe usuario ' + request.body.login+' ou a senha está incorreta.<br><br><a href="/">Voltar</a>');
      }
    });
  });
});

//adiciona página de cadastro de usuário
app.get('/cadastro',(request, response) => {
  response.sendFile(path.join(__dirname,'./views/cadastro.html'));
});

//adiciona novo usuario
app.post('/adduser',(request, response) => {
  banco.serialize(() => {
  	banco.run(incluirUsuario,[request.body.nome,request.body.email,request.body.login,request.body.senha], (err) => {
  	  if(err){
  	  	response.send('Erro no acesso ao banco de dados');
  	  	return console.log(err.message);
  	  }
  	  console.log('Usuario cadastrado');
  	  response.send('Usuario '+request.body.nome+' cadastrado com sucesso.<br><br><a href="/">Entrar</a>');
  	});
  });
});

//página que carrega os dados cadaatrados
app.get('/dados',(request, response) => {
  if(usuario != ''){
    banco.serialize(() => {
      banco.all(consultarDados,[], (err,rows) => {
        if(err){
          response.send('Erro no acesso ao banco de dados');
          return console.log(err.message);
        }
        dados = [];
        rows.forEach((row)=>{
          let item = {descricao:row.descricao,valor:row.valor};
          dados.push(item);
        });
      });
    response.render("dados",{itens:dados});
    });
  }else{
    response.redirect('/');
  }
});


//adiciona arquivo existente no diretório raiz
//esta é a página que vai aparecer ao acessar a aplicação
app.get('/insere',(request, response) => {
  if(usuario != ''){
    response.sendFile(path.join(__dirname,'./views/insere.html'));
  }else{
    response.redirect('/');
  }
});

//adiciona novo item
app.post('/additem',(request, response) => {
  banco.serialize(() => {
    banco.run(incluirDados,[request.body.descricao,request.body.valor], (err) => {
      if(err){
        response.send('Erro no acesso ao banco de dados');
        return console.log(err.message);
      }
      console.log('Item cadastrado');
      response.send('Item '+request.body.descricao+' cadastrado com sucesso.<br><br><a href="dados">Visualizar dados</a>');
    });
  });
});

//desvincula o usuário 
app.get('/logout',(request, response) => {
    usuario = '';
    response.redirect('/');
});


//fecha o banco
app.get('/close',(request, response) => {
	banco.close((err) => {
		if(err){
			response.send('Nao foi possivel encerrar a conexao');
			return console.log(err.message);
		}
		console.log('Conexao encerrada');
		response.send('Conexao com banco de dados encerrada');
	});
});


var estacoes = [];
//le arquivo csv
function leArquivo(){
  fs.createReadStream('dados/estacoes-bikesampa.csv')
    .on('error',()=> {})
    .pipe(csv())
    .on('data',(row) => estacoes.push(row))
    .on('end', () => {
      console.log('terminou');
    });
}

//página que carrega os dados cadaastrados
app.get('/estacoes',(request, response) => {
    leArquivo();
    response.status(200).json(estacoes);
});

app.listen(porta, () => {
  console.log(`Servidor usando Express rodando na porta ${porta}!`);
});