var divMapa = document.getElementById('mapa');

var zoom = 13;
var spCoord = [-23.533773, -46.625290];
var circleCoord = [-23.55567, -46.6292];
var iconCoord = [-23.534904, -46.595936];
var listaCoord = [[-23.539783, -46.644344],[-23.55552, -46.604347],[-23.531756, -46.66357],[-23.517434, -46.608467],[-23.571726, -46.615505]];
var listaHeat = [[-23.544189, -46.661339],[-23.533173, -46.662197],[-23.534275, -46.647606],[-23.551, -46.6508],[-23.554, -46.5906],[-23.554, -46.591],[-23.55487, -46.590612],[-23.55485, -46.590611],[-23.55483, -46.59061]];
var tile = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var copyright = 'OpenStreetMap, https://wiki.openstreetmap.org';

var map = L.map('mapa').setView(spCoord,zoom);
L.tileLayer(tile, {attribution: copyright}).addTo(map);

L.marker(spCoord).bindPopup('Informações sobre o marcador').addTo(map);

var circleStyle = {
	color: 'red',
	fillColor: 'blue',
	fillOpacity: 0.5,
	radius: 300
}

L.circle(circleCoord,circleStyle).bindTooltip('Raio de 300m').addTo(map);


//clique no mapa
var popup = L.popup();

function cliqueNoMapa(evento){
	popup.setLatLng(evento.latlng).setContent('Clique em :' + evento.latlng.toString()).openOn(map);
}

map.on('click',cliqueNoMapa);


//personalizando icones no mapa

var predio = L.icon({
	iconUrl: 'images/building.png',
	iconSize: [48,64],
	iconAnchor: [20,30],
	popupAnchor: [45,0]
});

L.marker(iconCoord,{icon: predio}).bindTooltip('Predio').addTo(map);


//adicionar lista

//listaCoord.forEach(adicionarNoMapa);

function adicionarNoMapa(item){
	L.marker(item,{icon: predio}).addTo(map);
}

var heatConfig = {
	radius: 100,
	minOpacity: 0.3
};

var heatmap = L.heatLayer(listaHeat,heatConfig).addTo(map);


//carregango JSON no mapa

var estacoesURL = 'estacoes';
var xhr = new XMLHttpRequest();

var estiloEstacoes = {
	color: 'red',
	fillColor: 'red',
	fillOpacity: 1.0,
	radius: 5
}


xhr.onreadystatechange = function(){
	if(this.readyState == 4 && this.status == 200){
		let listaDeEstacoes = JSON.parse(this.response);
		colocarNoMapa(listaDeEstacoes);
	}
}

function carregarEstacoes(){
	xhr.open('GET',estacoesURL,true);
	xhr.send();
}

function colocarNoMapa(listaEst){
	for(let i= 0; i < listaEst.length; i++){
		L.circle([listaEst[i].lat,listaEst[i].lon],estiloEstacoes).bindTooltip(listaEst[i].name).addTo(map);
	}
	alert('As estações foram incluídas no mapa')
}
